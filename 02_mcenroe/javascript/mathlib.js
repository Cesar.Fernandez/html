function rad(angulo){
    return angulo * Math.PI / 180
}

function p2c(radio, ang){
  return [radio * Math.cos(rad(ang)) , radio * Math.sin(rad(ang))]
}
